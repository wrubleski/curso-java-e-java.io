package br.com.alura.java_io.test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class TesteSerializacaoCliente {
	public static void main(String[] args) throws Exception {
//		Cliente cliente = new Cliente();
//
//		cliente.setNome("Henrique");
//		cliente.setProfissao("dev");
//		cliente.setCpf("123123");
//
//		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("cliente.bin"));
//
//		oos.writeObject(cliente);
//		oos.close();

		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("cliente.bin"));
		Cliente cliente2 = (Cliente) ois.readObject();
		ois.close();

		System.out.println(cliente2.getNome());
	}
}
