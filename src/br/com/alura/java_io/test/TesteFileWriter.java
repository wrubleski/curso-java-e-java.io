package br.com.alura.java_io.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TesteFileWriter {
	public static void main(String[] args) throws IOException {

		BufferedWriter bw = new BufferedWriter(new FileWriter("lorem2.txt"));

		bw.write("sunt in culpa qui officia deserunt mollit anim");
		bw.newLine();
		bw.write("asdfasdfasdfasdf asdf asd afsd asdf asdfasdfasd ");

		bw.close();
	}
}
