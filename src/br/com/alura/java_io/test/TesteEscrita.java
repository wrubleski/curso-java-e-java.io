package br.com.alura.java_io.test;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class TesteEscrita {

	public static void main(String[] args) throws IOException {

		OutputStream fos = new FileOutputStream("lorem2.txt");
		Writer osw = new OutputStreamWriter(fos);
		BufferedWriter bw = new BufferedWriter(osw);

		bw.write("sunt in culpa qui officia deserunt mollit anim");
		bw.newLine();
		bw.write("asdfasdfasdfasdf asdf asd afsd asdf asdfasdfasd ");

		bw.close();
	}

}
